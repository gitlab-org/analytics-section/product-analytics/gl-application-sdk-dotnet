# GitLab Application SDK - .NET

This SDK is for using GitLab Application Services with .NET applications.

Supports .NET 6.0 and above.

## How to use SDK

Install the SDK using NuGet:

```bash
dotnet add package GitLab.Sdk
```

Initialize SDL client in the application:

```csharp
GitlabTracker tracker = new (appId, host, port);
```

SDK client options:

| Param | Type   | Description                                                                                                            |
|-------|--------|------------------------------------------------------------------------------------------------------------------------|
| appId | string | The ID specified in the GitLab Project Analytics setup guide. It ensures your data is sent to your analytics instance. |
| host  | string | The GitLab Project Analytics instance specified in the setup guide.                                                    |
| port  | int    | The port of the GitLab Project Analytics instance.                                                                     |

## Methods

### Track

Sends a tracking event to GitLab Project Analytics.

```csharp
tracker.track(eventName, eventAttributes)
```

| Param           | Type                       | Description                                    |
|-----------------|----------------------------|------------------------------------------------|
| eventName       | string                     | The name of the event you want to track.       |
| eventAttributes | Dictionary<string, object> | The attributes of the event you want to track. |

This method doesn't include user data. To track user data, use the `Identify` method before calling tracking method.

### Identify

Used to associate a user and their attributes with the session and tracking events.

```csharp
tracker.identify(userId, userAttributes)
```

| Param           | Type                       | Description                                    |
|-----------------|----------------------------|------------------------------------------------|
| userId          | string                     | The ID of the user you want to identify.       |
| *userAttributes | Dictionary<string, object> | The attributes of the user you want to track.  |

*optional

### EnsureAllSent

Static method to ensure emitters were correctly shut down

```csharp
Client.EnsureAllSent()
```


## Example Web app

`DemoWebApp` is a sample web application that uses the GitLab Application SDK.

To run the sample web application:

1. Start snowplow-micro instance to collect events:

```bash
docker run -p 9090:9090 -e MICRO_IGLU_REGISTRY_URL=https://gitlab-org.gitlab.io/analytics-section/product-analytics/iglu snowplow/snowplow-micro:latest
```

2. `dotnet run` in `DemoWebApp` directory
3. Execute get request to either of endpoints:

```bash
curl http://localhost:5000 # for event tracking
curl http://localhost:5000/123456 # for event with additional user data
```
