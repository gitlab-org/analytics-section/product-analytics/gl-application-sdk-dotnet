using GitlabSdk.Storage;

namespace Sdk.Tests.Storage;

public class MemoryTests
{
    private Memory _memory = new();

    [Fact]
    public void TestPutAndTotalItems()
    {
        _memory = new Memory();
        
        _memory.Put("test1");
        _memory.Put("test2");
        
        Assert.Equal(2, _memory.TotalItems);
    }
    
    [Fact]
    public void TestTakeAndDelete()
    {
        _memory = new Memory();
        
        _memory.Put("test1");
        _memory.Put("test2");

        var deleteList = _memory.TakeLast(2);
        
        var idList = deleteList.Select(r => r.Id).ToList();
        _memory.Delete(idList);
        
        Assert.Equal(0, _memory.TotalItems);
    }
    
    [Fact]
    public void TestTakeLast()
    {
        _memory = new Memory();
        
        _memory.Put("test1");
        _memory.Put("test2");
        
        var records = _memory.TakeLast(2);
        
        Assert.Equal(2, records.Count);
        Assert.Equal("test1", records[0].Item);
        Assert.Equal("test2", records[1].Item);
    }
}