using Snowplow.Tracker;
using GitlabSdk;
using Newtonsoft.Json.Linq;

namespace Sdk.Tests;

public class GitlabTrackerTests : IDisposable
{
    private readonly GitlabTracker _gitlabTracker = new("test", "localhost", 9090);

    [Fact]
    public void TrackerStartedAfterInitialization()
    {
        Assert.True(Tracker.Instance.Started);
    }

    [Fact]
    public void TrackerStoppedAfterStopping()
    {
        GitlabTracker.EnsureAllSent();

        Assert.False(Tracker.Instance.Started);
    }

    [Fact]
    public void TracksAnonymousEvent()
    {
        _gitlabTracker.Track("event_a", new { tygr = "sushi" });

        var context = ExtractFieldFromLastRecord("ue_px");
        var customCtx = Decode64(context);

        Assert.Matches(@"""tygr"":""sushi""", customCtx);
        Assert.Matches(@"""name"":""event_a""", customCtx);
    }

    [Fact]
    public void TracksAnonymousEventWithNoPayload()
    {
        _gitlabTracker.Track("event_a", null);

        var context = ExtractFieldFromLastRecord("ue_px");
        var customCtx = Decode64(context);

        dynamic data = JObject.Parse(customCtx)["data"]["data"];

        Assert.Null(data["props"].Value);
        Assert.Equal(data["name"].Value, "event_a");
    }

    [Fact]
    public void TracksIdentifiedEventWithNoAttributes()
    {
        const string targetUser = "abcd";

        _gitlabTracker.Identify(targetUser);
        _gitlabTracker.Track("event_a", null);

        var userId = ExtractFieldFromLastRecord("uid");

        Assert.Equal(targetUser, userId);
    }

    [Fact]
    public void TracksIdentifiedEventWithAttributes()
    {
        _gitlabTracker.Identify("abcd", new Dictionary<string, object> { { "name", "John" } });
        _gitlabTracker.Track("event_a", null);

        var ctx = ExtractFieldFromLastRecord("cx");
        var userCtx = Decode64(ctx);

        Assert.Matches(@"""name"":""John""", userCtx);
    }

    private string Decode64(string source)
    {
        var base64 = Convert.FromBase64String(source);
        return System.Text.Encoding.UTF8.GetString(base64);
    }

    private string ExtractFieldFromLastRecord(string fieldName)
    {
        var json = JObject.Parse(_gitlabTracker.Memory.TakeLast(1).First().Item);
        return json.First.First[fieldName].ToString();
    }

    public void Dispose()
    {
        if (Tracker.Instance.Started)
        {
            GitlabTracker.EnsureAllSent();
        }
    }
}
