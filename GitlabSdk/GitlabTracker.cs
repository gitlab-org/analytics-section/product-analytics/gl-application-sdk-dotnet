﻿using GitlabSdk.Storage;
using Snowplow.Tracker;
using Snowplow.Tracker.Emitters;
using Snowplow.Tracker.Endpoints;
using Snowplow.Tracker.Models;
using Snowplow.Tracker.Models.Events;
using Snowplow.Tracker.Models.Adapters;
using Snowplow.Tracker.Queues;
using Snowplow.Tracker.Models.Contexts;
using Snowplow.Tracker.Logging;
using Snowplow.Tracker.Storage;
using HttpMethod = Snowplow.Tracker.Endpoints.HttpMethod;

namespace GitlabSdk
{
  /// <summary>
  /// Class <c>GitlabTracker</c> is a event tracking client.
  /// </summary>
  public class GitlabTracker
  {
    private const string CustomEventSchema = "iglu:com.gitlab/custom_event/jsonschema/1-0-0";
    private const string UserContextSchema = "iglu:com.gitlab/user_context/jsonschema/1-0-0";
    private const string DefaultTrackerNamespace = "gitlab";

    private readonly Subject _subject = new Subject().SetPlatform(Platform.Web).SetLang("EN");
    private IContext? _userContext;
    private readonly IStorage _storage = new Memory();

    /// <param name="appId">The ID specified in the GitLab Project Analytics setup guide. It ensures your data is sent to your analytics instance.</param>
    /// <param name="host">The GitLab Project Analytics instance specified in the setup guide.</param>
    /// <param name="port">The port of the GitLab Project Analytics instance.</param>
    public GitlabTracker(string appId, string host, int port)
    {
      if (Tracker.Instance.Started)
      {
        return;
      }

      var logger = new ConsoleLogger();
      var endpoint = new SnowplowHttpCollectorEndpoint(host, method: HttpMethod.POST, l: logger, port: port);
      var queue = new PersistentBlockingQueue(_storage, new PayloadToJsonString());
      var emitter = new AsyncEmitter(endpoint, queue, l: logger);
      var session = new ClientSession("client_session.dict");

      Tracker.Instance.Start(emitter: emitter, subject: _subject, clientSession: session,
        trackerNamespace: DefaultTrackerNamespace, appId: appId, encodeBase64: true, l: logger);
    }

    /// <summary>
    /// Sends a tracking event to GitLab Project Analytics.
    /// </summary>
    /// <param name="eventName">The name of the event you want to track.</param>
    /// <param name="eventPayload">The attributes of the event you want to track.</param>
    public void Track(string eventName, object eventPayload)
    {
      var eventDict = new Dictionary<string, object>
      {
        { "name", eventName },
        { "props", eventPayload }
      };

      var selfDescJson = new SelfDescribingJson(CustomEventSchema, eventDict);

      if (_userContext != null)
      {
        var contexts = new List<IContext>() { _userContext };
        Tracker.Instance.Track(new SelfDescribing().SetEventData(selfDescJson).SetCustomContext(contexts).Build());
      }
      else
      {
        Tracker.Instance.Track(new SelfDescribing().SetEventData(selfDescJson).Build());
      }
    }

    /// <summary>
    /// Used to associate a user and their attributes with the session and tracking events.
    /// </summary>
    /// <param name="userId">The ID of the user you want to identify.</param>
    public void Identify(string userId)
    {
      _subject.SetUserId(userId);
    }

    /// <summary>
    /// Used to associate a user and their attributes with the session and tracking events.
    /// </summary>
    /// <param name="userId">The ID of the user you want to identify.</param>
    /// <param name="userAttributes">The attributes of the user you want to track.</param>
    public void Identify(string userId, Dictionary<string, object> userAttributes)
    {
      Identify(userId);

      _userContext = new GenericContext()
        .SetSchema(UserContextSchema)
        .AddDict(userAttributes)
        .Build();
    }

    /// <summary>
    /// Static method to ensure emitters were correctly shut down
    /// </summary>
    public static void EnsureAllSent()
    {
      Tracker.Instance.Flush();
      Tracker.Instance.Stop();
    }

    public IStorage Memory => _storage;
  }
}
