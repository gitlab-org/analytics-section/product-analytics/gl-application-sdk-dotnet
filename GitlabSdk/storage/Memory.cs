using Snowplow.Tracker.Storage;

namespace GitlabSdk.Storage
{
  public class Memory : IStorage
  {
    private readonly List<StorageRecord> _records = new();
    private readonly object _root;

    public Memory()
    {
      _root = ((System.Collections.ICollection)_records).SyncRoot;
    }

    public int TotalItems
    {
      get
      {
        lock (_root)
        {
          return _records.Count();
        }
      }
    }

    public bool Delete(List<string> idList)
    {
      var failedDeletion = false;
      lock (_root)
      {
        foreach (var record in idList.Select(id => _records.FirstOrDefault(r => r.Id == id)))
        {
          if (record != null)
          {
            _records.Remove(record);
          }
          else
          {
            failedDeletion = true;
          }
        }
      }
      return !failedDeletion;
    }

    public void Put(string item)
    {
      lock (_root)
      {
        _records.Add(new StorageRecord
        {
          Id = Guid.NewGuid().ToString(),
          Item = item
        });
      }
    }

    public List<StorageRecord> TakeLast(int n)
    {
      lock (_root)
      {
        return _records.TakeLast(n).ToList();
      }
    }
  }
}
