using GitlabSdk;
using Microsoft.AspNetCore.Mvc;

namespace DemoApplication.Controllers;

[ApiController]
[Route("")]
public class UserActionController : ControllerBase
{
    public static string appId = Environment.GetEnvironmentVariable("PA_APPLICATION_ID") ?? "test_app";
    public static string collectorHost = Environment.GetEnvironmentVariable("PA_COLLECTOR_HOST") ?? "localhost";
    public static int collectorPort = Int32.Parse(Environment.GetEnvironmentVariable("PA_COLLECTOR_PORT") ?? "9090");

    private readonly GitlabTracker _tracker = new (appId, collectorHost, collectorPort);
    
    public OkResult Get()
    {
        _tracker.Track("ping_view", new Dictionary<string, object> { { "pageName", "Home" } });
        
        return Ok();
    }
    
    [Route("{userId:int}")]
    public OkResult Get(int userId)
    {
        _tracker.Identify(userId.ToString(), new Dictionary<string, object> { { "name", "Test User" } });
        _tracker.Track("ping_view", new { });
        
        return Ok();
    }
}
